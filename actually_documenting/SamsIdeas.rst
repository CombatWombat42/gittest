Git work flow
-------------

I believe that the branching models mentioned `here <http://nvie.com/posts/a-successful-git-branching-model/>`_ is probably a fairly good one.

* my intention would be for pull request from developers to their reviewers go to the "develop" branch and then SCCB copies them from "develop" to "production". 
* my belief is that we can restrict the server in such a way that SQA need not verify after SCCB has done it's job.
* we need to look at `fast forwarding <https://sandofsky.com/images/fast_forward.pdf>`_ but my guess is that we will not want to do it.
Developer
----------

Working
=========

#. Clone repo
#. Get assigned ticket/work.
#. Pull "develop" and "production `git pull <origin(TBR)>` to make sure I am current
#. Switch to "develop" `git checkout develop`
#. Create a branch for this ticket `git checkout -b <my_ticket_number_and_anything_else_I_want>`
#. Hack away, pushing my branch to the server daily. `git push origin <my_ticket_number_and_anything_else_I_want>`
#. Finish code
#. Document testing
#. Merge "develop" onto my branch `git merge develop`
#. Resolve conflicts.
#. Create pull request for my branch onto develop with my assigned reviewer.
#. Hope the reviewer gets to it in time so there is no merge conflict.

Reviewing
==========

#. Get assigned review
#. `Fight other developers with cardboard tube <https://xkcd.com/303/>`_
#. Do review
#. Complete review
#. Approve review
#. Merge review see merge conflict
#. Return ticket to original developer

Working 2
===============

#. Roll eyes at reviewer 
#. Say `I told you so <https://cdn.meme.am/instances/500x/37802796.jpg>`_
#. Merge "develop" onto my branch `git merge develop`
#. Resolve conflicts.
#. Create pull request for my branch onto develop with my assigned reviewer.
#. Hope the reviewer gets to it in time so there is no merge conflict.

Reviewing 2
=============
#. `Throw a light switch rave <https://www.youtube.com/watch?v=CU5mBpFD8GQ>`_
#. Approve review
#. Merge review

SCCB
-------

#. Look at pull request assigned to it
 * Assure they are not going anywhere but from "develop" to "production"
#. Approve
#. Merge

Things we need controlled
----------------------------

* Pull requests must only be able to be merged once all approvals are done.
 * Auto merging would be nice. 
* Need to disable committing to "master" and "develop" this will need to be client side.
